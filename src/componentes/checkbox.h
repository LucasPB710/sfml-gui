#pragma once

class gCheckbox{

public:
  gCheckbox(float s, float x, float y);

  sf::RectangleShape box;

private:

  float posX;
  float posY;
  //é um quadrado, size é o tamanho de cada lado
  float size;
};
