#include "button.h"
#include <SFML/Graphics.hpp>

gButton::gButton(float x_size, float y_size, float x_pos, float y_pos){
    setSize(x_size, y_size);
    setPos(x_pos, y_pos);
}


bool gButton::is_clicked(sf::RenderWindow &win){
    if(sf::Mouse::getPosition(win).x > getPos().x && sf::Mouse::getPosition(win).x < getPos().x + tamX
    && sf::Mouse::getPosition(win).y >= getPos().y && sf::Mouse::getPosition(win).y <= getPos().y + tamY && sf::Mouse::isButtonPressed(sf::Mouse::Left)){
        return true;
    }

    else{
        return false;
    }

}

bool gButton::is_hovered(sf::RenderWindow &win){
    if(sf::Mouse::getPosition(win).x > getPos().x && sf::Mouse::getPosition(win).x < getPos().x + tamX
    && sf::Mouse::getPosition(win).y >= getPos().y && sf::Mouse::getPosition(win).y <= getPos().y + tamY){
        return true;
    }
    else
        return false;

}



void gButton::setSize(float x, float y){
    tamX = x;
    tamY = y;
    button.setSize(sf::Vector2f(x,y));
}

void gButton::setPos(float x, float y){
    posX = x;
    posY = y;
    button.setPosition(x,y);
}

void gButton::setColor(int r, int g, int b){
    button.setFillColor(sf::Color(r,g,b));
}

sf::Vector2f gButton::getSize(){return sf::Vector2f(tamX,tamY);}
sf::Vector2f gButton::getPos(){return sf::Vector2f(posX,posY);}
