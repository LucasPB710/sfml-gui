#pragma once
#include <SFML/Graphics.hpp>
#include <iostream>

class gWindow{
public:

  gWindow(int x, int y, std::string title_window);

  void render();
  void draw(auto coiso);
  void clear();

  bool is_open();

  // sf::RenderWindow getWindow();
  //sf::RenderWindow window(sf::VideoMode(tamX, tamY), title);
  //
  sf::RenderWindow *window = new sf::RenderWindow(sf::VideoMode(800,600), "test");

private:
  int tamX = 800;
  int tamY = 600;

  std::string title = "test";


};
