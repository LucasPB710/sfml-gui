#pragma once
#include <SFML/Graphics.hpp>

class gButton{

public:

  gButton(float x_size, float y_size, float x_pos, float y_pos);

  bool is_clicked(sf::RenderWindow &win);
  bool is_hovered(sf::RenderWindow &win);
  void setSize(float x, float y);
  void setPos(float x, float y);
  void setColor(int r, int g, int b);

  sf::Vector2f getSize();
  sf::Vector2f getPos();

  sf::RectangleShape button;

  // float getTamX();
  // float getTamY();
  // float getPosX();
  // float getPosY();

private:
  float tamX;
  float tamY;

  float posX;
  float posY;
};
