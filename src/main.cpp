#include <iostream>
#include <SFML/Graphics.hpp>
#include "componentes/button.h"
#include "componentes/window.h"
#include "componentes/checkbox.h"

int main(){

    gWindow janela(800, 600, "Teste!");
    gButton botao(100, 100, 0, 0);
    botao.setColor(255, 0, 0);

    while(janela.is_open()){
        janela.clear();
        //janela.draw(botao.button);
        janela.window->draw(botao.button);
        janela.render();
    }

    return 0;
}
