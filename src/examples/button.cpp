#include <iostream>
#include <sfml-gui/window.h>
#include <sfml-gui/button.h>
/*
To compile use:

g++ button.cpp -lsfml-graphics -lsfml-window -lsfml-system -lbutton -lwindow -o button

*/

int main(){

    gWindow janela(800, 600, "CLICK THE BUTTON!");
    gButton button(100, 100, 0, 0);
    button.setColor(255, 0, 0);

    while(janela.is_open()){
        janela.clear();
        janela.window->draw(button.button);
        janela.render();

        if(button.is_clicked(*janela.window))
            std::cout<<"Button clicked!\n";
    }

    return 0;
}
